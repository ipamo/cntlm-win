param(
    [Parameter(Position = 0)]
    [string] $Command,
    [switch] $Retries
)

$InformationPreference = "Continue"
$ErrorView = "CategoryView"

$Debug = $PSCmdlet.MyInvocation.BoundParameters["Debug"].IsPresent
$RootDir = $PSScriptRoot
$CntlmExe = "$RootDir\bin\cntlm.exe"
$Env:CYGWIN = "nodosfilewarning"

if (-Not (Test-Path "$CntlmExe")) {
    Write-Error "Cntlm not found: $CntlmExe"
    exit 1
}

function DetectConfig {
    Write-Verbose "Detect config"
    foreach ($Filename in Get-ChildItem -Path "$RootDir\config" -Filter "*.ini" -File -Name) {
        $Name = ([regex] "^(.*)\.ini$").Match($Filename).Groups[1].Value
        if ($Name -eq "noproxy") {
            continue
        }
        
        Write-Verbose "Parse config $Name.ini"

        # Parse proxy
        $Proxy = ""
        foreach ($line in Get-Content "$RootDir\config\$Filename") {
            $match = ([regex] "^\s*Proxy\s*([^#]+)").Match($line)
            if ($match.Success) {
                $Proxy = $match.Groups[1].Value.Trim()
                break
            }
        }
        
        if ($Proxy) {
            # Determine server name
            $match = ([regex] "^([^\:]+)").Match($Proxy)
            if ($match.Success) {
                $Server = $match.Groups[1].Value.Trim()
            }
            else {
                $Server = $Proxy
            }

            Write-Verbose "Test connection to $Server (Proxy=$Proxy)"
           if (Test-Connection "$Server" -Count 1 -Delay 2 -Quiet) {
               return "$Name"
           }
        }
    }

    return "noproxy"
}

function StartCntlm {
    param(
        [Parameter(Mandatory = $true)]
        [string] $Name
    )

    $CntlmArgs = "-c", "$RootDir\config\$Name.ini"
    
    if ($Debug) {
        $CntlmArgs += "-v"
    }

    $RetriesTotal = if ($Retries) { 5 } else { 0 }
    $RetriesCount = 0
    while ($true) {
        $Message = "Start cntlm with $Name.ini"
        if ($RetriesCount -gt 0) {
            $Message += " (retry $RetriesCount/$RetriesTotal)"
        }
        Write-Information $Message
        & "$CntlmExe" @CntlmArgs
        if ($LastExitCode -eq 0) {
            return
        }
        elseif ($RetriesCount -eq $RetriesTotal) {
            Write-Error "Cntlm exited with code $LastExitCode"
            return
        }
        else {
            Write-Host "Cntlm exited with code $LastExitCode : will try again in a few seconds..." -ForegroundColor Yellow
            Start-Sleep -Seconds 2
            $RetriesCount += 1
        }
    }
}

function StopCntlm {
    Write-Verbose "Stop any cntlm process"
    $ps = Get-Process | Where-Object {$_.Name -eq "cntlm"}
    foreach ($p in $ps) {
        Write-Information "Stop $($p.Name) (PID $($p.Id))"
        Stop-Process -Id $p.Id
    }
}

# --------------------------------------------------------------------------
if ($Command -eq "list") {
    Write-Verbose "List running cntlm process"
    Get-Process | Where-Object {$_.Name -eq "cntlm"}
}
# --------------------------------------------------------------------------
elseif ($Command -eq "stop") {
    StopCntlm
}
# --------------------------------------------------------------------------
else {
    if ($Command) {
        # $Command is the config name
        $Name = $Command
    }
    else {
        # No name given: try to detect which config to use
        $Name = DetectConfig
    }

    StopCntlm
    StartCntlm -Name "$Name"
}
