cntlm-win
=========

Simplify usage of [Cntlm authenticating proxy](http://cntlm.sourceforge.net) on modern Windows: easy configuration and switch between corporate and home environments.

## Quick Start

1. **Create a config file for your corporate proxy**, for example: `config\mycorp.ini`.

   You may use `examples\example.ini` as a base, and adapt _YourProxy_, _YourDomain_ and _YourUsername_.

   Launch `.\bin\cntlm.exe -H -c .\config\mycorp.ini` to generate the password block.

2. **Run `.\cntlm.ps1`** in a Powershell window.

   The script will automatically ensure that your corporate proxy is available, and start Cntlm intermediate proxy
   to handle authentication for your corporate proxy.
   
3. **Configure your applications** to use the Cntlm proxy on `127.0.0.1`, port `3128`.

4. **Run `.\cntlm.ps1` again whenever your environment change** (for example when you take your corporate PC at home). 

   The script will detect whether your corporate proxy is still available or not.
   If not, it will make Cntlm forward all requests directly to the Internet (instead of through your corporate proxy).
   In any case, Cntlm will still be available: there is no need to change your applications settings.

## How to create a shortcut

Windows context menu > New > Shortcut
- Target: `powershell.exe -Noexit -ExecutionPolicy Bypass -File full/path/to/cntlm.ps1 -Retries`

## Credits

Origin of binary files:
- `bin\cntlm.exe`: Cntlm 0.92.3 [[download]](https://sourceforge.net/projects/cntlm/files/cntlm/cntlm%200.92.3/cntlm-0.92.3-win32.zip/download)
- `bin\cygwin1.dll`: Cygwin 3.1.4-1 [[download]](http://cygwin.mirror.constant.com/x86/release/cygwin/cygwin-3.1.4-1.tar.xz)

**CNTLM - Authenticating HTTP Proxy**  
Copyright (C) 2007-2010 David Kubicek.  
Licensed under the GNU General Public License (GPL), version 2.  
Available at: http://cntlm.sourceforge.net  

**Cygwin**  
Copyright Free Software Foundation and other contributors.  
Licensed under the GNU Lesser General Public License (LGPL), version 3 or later.  
Available at: https://cygwin.com  
